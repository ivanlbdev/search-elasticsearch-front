import React, { useEffect, useState } from 'react'
import { API } from '../utils/api'
import AListItmes from '../components/AListItmes'
import AInput from '../components/AInput'
import AFullSearch from '../components/AFullSearch'

import '../assets/scss/a-search.scss'
import AButton from '../components/AButton'

const ASearch = () => {
    const [searchText, setSearch] = useState('')
    const [result, setResult] = useState({})
    const [fullResult, setFullResult] = useState({})
    let text = ''

    const goSearch = async (event, from = 1) => {
        setSearch(event.target.value)
        const params = {
            text: event.target.value,
            count: 5,
            from_search: from
        }
        text = event.target.value
        if (text !== '') {
            const res = await API.getPreSearch(params)
            setResult(res.data)
        }
    }

    const fullSearch = async(from = 1) => {
        console.log(searchText)
        if (searchText !== '') {
            const params = {
                count: 15,
                text: searchText,
                full: 1,
                from_search: from
            }
            const res = await API.getPreSearch(params)
            console.log(res)
            setFullResult(res)
        }
        console.log(fullResult)
    }

    let listItems = ''
    let listBrands = ''
    let listCategories =''
    if (searchText !== '' && result) {
        listItems = <AListItmes items={result['products']} title={"Товары:"} />
        listBrands = <AListItmes items={result['brands']} title={"Бренды:"} />
        listCategories = <AListItmes items={result['categories']} title={"Категории:"} />
    }

    let listFullSearch = ''
    let allSearchTotal = ''
    if (Object.keys(fullResult).length > 0) {
        listFullSearch = <AListItmes items={fullResult['data']['products']} full={true} total={fullResult['data']['total']['value']} pagination={fullSearch} />
        allSearchTotal = <p>Всего найдено: {fullResult['data']['total']['value']}</p>
    }

    return (
        <div className='container'>
            <div className='container__item container__item_40'>
                <div className='container__search-input'>
                    <AInput searchText={searchText} search={goSearch} searchKey={fullSearch} />
                    <AButton title={'Полный поиск'} search={fullSearch} />
                </div>
                {listBrands}
                {listCategories}
                {listItems}
            </div>
            <div className='container__item'>
                <div>
                    <AFullSearch />
                    {allSearchTotal}
                </div>
                {listFullSearch}
            </div>
        </div>
    )
}

export default ASearch