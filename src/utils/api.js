import axios from 'axios'

const baseUrl = process.env.REACT_APP_URL_API

const config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
    }
}


export class API {

    static async getPreSearch(params) {
        console.log(params)
        const response = await axios.get(`http://127.0.0.1:8000/api/search/`, {
            params
        })
        return response
    }

    static async getFullSearch(text) {
        const response = await axios.get(`http://127.0.0.1:8000/api/search/full/${text}`)
        return response
    }

}
