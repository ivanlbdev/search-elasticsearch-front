export class Pagination {
    #pages = []
    #current = 1
    #count = 0
    #around = 1


    get(config = {}) {
        const { current, count, around } = config

        this.#pages = []
        this.#current = current >= 1 ? Number.parseInt(current, 10) : 1
        this.#count = count >= 0 ? Number.parseInt(count, 10) : 0
        this.#around = around >= 0 ? Number.parseInt(around, 10) : 1

        this.prepare()

        return this.#pages
    }


    prepare() {
        if (!(this.#count > 0)) {
            return
        }

        this.#current = this.#current >= 1 && this.#current <= this.#count ? this.#current : 1
        const set = new Set([1, this.#current, this.#count])

        let start = this.#current - this.#around
        start = start >= 1 ? start : 1

        let end = this.#current + this.#around
        end = end <= this.#count ? end : this.#count

        if (this.#current === 1) {
            end += 2
        } else if (this.#current === 2) {
            end += 1
        }

        if (this.#current === this.#count) {
            start -= 2
        } else if (this.#current === this.#count - 1) {
            start -= 1
        }

        if (start - 1 === 2) {
            --start
        }

        if (this.#count - end === 2) {
            ++end
        }

        for (let page = start; page <= end; ++page) {
            set.add(page)
        }

        const result = [...set].filter((p) => p >= 1 && p <= this.#count).sort((a, b) => a - b)

        if (result.length > 1) {
            if (result?.[1] - result?.[0] !== 1) {
                result.splice(1, 0, '...')
            }

            if (result?.[result.length - 1] - result?.[result.length - 2] !== 1) {
                result.splice(result.length - 1, 0, '...')
            }
        }

        this.#pages = result
    }

}