import React from 'react'

import '../assets/scss/a-input.scss'

const AInput = ({ searchText, search, searchKey}) => {

    return (
        <div>
            <input
                className='main-input'
                placeholder={"Поиск..."}
                value={searchText}
                type="text"
                onChange={(event) => search(event)}
                onKeyPress={(event) => {
                    if (event.key === 'Enter' || event.keyCode === 13) {
                        searchKey()
                    }
                }}
            />
        </div>
    )
}

export default AInput