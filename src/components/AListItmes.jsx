import React from 'react'

import '../assets/scss/a-list-items.scss'
import APagination from "./APagination";

const AListItmes = (props) => {
    console.log(props)

    const isFull = props.full || false

    let pag = ''

    if (isFull) {
        pag = <APagination total={props.total} click={props.pagination} />
    }

    return (
        <div className='list-items'>
            <h4>{props.title || ''}</h4>
            {props?.items?.map((item, index) =>
                item.highlight
                    ? <p key={index} dangerouslySetInnerHTML={{__html: item['highlight']['title.cyrillic_ngram']}}/>
                    : <p key={index}>{item['_source']['title']}</p>
            )}

            {pag}

        </div>
    )
}

export default AListItmes