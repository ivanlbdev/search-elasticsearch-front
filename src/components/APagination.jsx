import React, { useEffect, useState } from 'react'
import AButton from './AButton'
import { Pagination } from '../utils/pagination'
import '../assets/scss/a-pagination.scss'


const APagination = (props) => {

    const [currentPage, setCurrentPage] = useState(1)
    let currentActualPage = 1
    const itemsPerPage = 15
    console.log(props.total)
    const totalPages = Math.ceil(+(props.total) / itemsPerPage)

    let truePag = new Pagination()
    useEffect(() => {
        truePag = new Pagination()
    }, [currentPage])

    const config = {
        current: currentPage,
        count: totalPages
    }
    console.log(truePag.get(config))

    return (
        <div className='main-pagination' style={{display: 'flex', flexWrap: 'wrap'}}>
            {truePag.get(config).map((item, index) =>
                item === '...'
                    ? <span className='main-pagination__separator'>{item}</span>
                    : <AButton key={index} title={item} active={item===currentPage} search={() => {
                        setCurrentPage(+item)
                        currentActualPage = +item
                        console.log(currentPage)
                        props.click((currentActualPage-1) * itemsPerPage)
                    }
                    } />
            )}
        </div>
    )
}

export default APagination