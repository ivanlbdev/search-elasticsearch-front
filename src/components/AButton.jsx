import React from 'react'

import '../assets/scss/a-button.scss'

const AButton = (props) => {

    const classNames = require('classnames')

    let buttonClass = classNames({
        'main-button': true,
        'main-button_active': props.active
    })

    return (
        <div>
            <button className={buttonClass} onClick={() => props.search()}>
                {props.title}
            </button>
        </div>
    )
}

export default AButton