# Frontend elasticsearch

## Backend приложения
Можно найти по ссылке `https://gitlab.com/ivanlbdev/search-elasticsearch-back`
## Сборка и запуск

Устанока зависимостей:
```shell
$ cd <папка_с_проектом> npm install
```

Запуск в dev режиме:
```shell
$ cd <папка_с_проектом> npm start
```
